#include <windows.h>
#include "Scene.hpp"
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>

Scene::Scene()
    : m_rootNode(new osg::Group)
{
    m_rootNode->addChild(new osg::Geode);
    m_rootNode->addUpdateCallback(this);
}

osg::Node* Scene::getRoot()
{
    return m_rootNode.get();
}

void Scene::operator()(osg::Node* node, osg::NodeVisitor* nv)
{
    unsigned refcount = 0;
    //check refcount
    if (static_cast<osg::Geode*>(m_rootNode->getChild(0))->getNumDrawables())
    {
        auto drawable = static_cast<osg::Geode*>(m_rootNode->getChild(0))->getDrawable(0);
        refcount = drawable->referenceCount();
    }

    UpdateScene();

    //check refcount
    if (static_cast<osg::Geode*>(m_rootNode->getChild(0))->getNumDrawables())
    {
        auto drawable = static_cast<osg::Geode*>(m_rootNode->getChild(0))->getDrawable(0);
        refcount = drawable->referenceCount();
    }
};

void Scene::UpdateScene() const
{
    auto childNode = static_cast<osg::Geode*>(m_rootNode->getChild(0));
    childNode->removeDrawables(0, childNode->getNumDrawables());

    osg::ref_ptr<osg::Geometry> geometry(new osg::Geometry);
    childNode->addDrawable(geometry);
}
