#ifndef SCENE_HPP
#define SCENE_HPP

#include <osg/ref_ptr>
#include <osg/NodeCallback>

namespace osg
{
    class Node;
    class Geode;
    class Group;
    class NodeCallback;
}

class Scene : public osg::NodeCallback
{
public:
    Scene();
    osg::Node* getRoot();

private:
    void operator()(osg::Node*, osg::NodeVisitor*) override;
    void UpdateScene() const;

    osg::ref_ptr<osg::Group> m_rootNode;
};

#endif //SCENE_HPP