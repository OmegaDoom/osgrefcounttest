#define _USE_MATH_DEFINES

#include <QtGui/QMainWindow>
#include <QtGui/QHBoxLayout>
#include <QtGui/QApplication>
#include <windows.h>
#include <osgViewer/api/win32/GraphicsWindowWin32>
#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osgGA/OrbitManipulator>
#include <osgDB/ReadFile>

#include "Scene.hpp"

osg::ref_ptr<osg::Camera> getCamera(QWidget& widget)
{
    auto id = widget.winId();
    osg::ref_ptr<osg::Referenced> windata =
        new osgViewer::GraphicsWindowWin32::WindowData(reinterpret_cast<HWND>(id));

    osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
    traits->x = 0;
    traits->y = 0;
    traits->width = 800;
    traits->height = 600;
    traits->windowDecoration = false;
    traits->doubleBuffer = true;
    traits->inheritedWindowData = windata;
    osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());
    osg::ref_ptr<osg::Camera> camera = new osg::Camera;
    camera->setGraphicsContext(gc);
    camera->setViewport(
        new osg::Viewport(0, 0, traits->width, traits->height));
    camera->setClearMask(GL_DEPTH_BUFFER_BIT |
        GL_COLOR_BUFFER_BIT);
    camera->setClearColor(osg::Vec4f(0.2f, 0.2f, 0.4f, 1.0f));
    camera->setProjectionMatrixAsPerspective(
        30.0f, (double)traits->width / (double)traits
        ->height, 1.0, 1000.0);

    camera->setCullingMode(camera->getCullingMode() & ~osg::CullSettings::SMALL_FEATURE_CULLING);
    return camera;
}

int main(int argc, char *argv[])
{
    osg::DisplaySettings::instance()->setNumMultiSamples(1);

    QApplication app(argc, argv);
    QWidget mainWindow;
    QHBoxLayout layout;
    QWidget window1;
    window1.setAttribute(Qt::WA_NativeWindow);
    window1.resize(200, 200);
    QWidget window2;
    Scene view;
    layout.addWidget(&window1);
    layout.addWidget(&window2);
    mainWindow.setLayout(&layout);

    auto camera = getCamera(window1);

    mainWindow.resize(840, 640);

    osg::ref_ptr<osgViewer::Viewer> g_viewer(new osgViewer::Viewer);
    g_viewer->setCamera(camera.get());
    osg::ref_ptr<osgGA::OrbitManipulator> cameraManipulator = new osgGA::OrbitManipulator();
    cameraManipulator->setHomePosition(osg::Vec3(0, 0, 0.01), osg::Vec3(0, 0, -1), osg::Vec3(1, 0, 0));
    g_viewer->setCameraManipulator(cameraManipulator, false);

    g_viewer->setSceneData(view.getRoot());

    mainWindow.show();

    while (mainWindow.isVisible())
    {
        g_viewer->frame();
        app.processEvents();
    }
}